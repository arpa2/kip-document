# This file was automatically generated by SWIG (https://www.swig.org).
# Version 4.1.0
#
# Do not make changes to this file unless you know what you are doing - modify
# the SWIG interface file instead.

from sys import version_info as _swig_python_version_info
# Import the low-level C/C++ module
if __package__ or "." in __name__:
    from . import _kipservice
else:
    import _kipservice

try:
    import builtins as __builtin__
except ImportError:
    import __builtin__

def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except __builtin__.Exception:
        strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)


def _swig_setattr_nondynamic_instance_variable(set):
    def set_instance_attr(self, name, value):
        if name == "this":
            set(self, name, value)
        elif name == "thisown":
            self.this.own(value)
        elif hasattr(self, name) and isinstance(getattr(type(self), name), property):
            set(self, name, value)
        else:
            raise AttributeError("You cannot add instance attributes to %s" % self)
    return set_instance_attr


def _swig_setattr_nondynamic_class_variable(set):
    def set_class_attr(cls, name, value):
        if hasattr(cls, name) and not isinstance(getattr(cls, name), property):
            set(cls, name, value)
        else:
            raise AttributeError("You cannot add class attributes to %s" % cls)
    return set_class_attr


def _swig_add_metaclass(metaclass):
    """Class decorator for adding a metaclass to a SWIG wrapped class - a slimmed down version of six.add_metaclass"""
    def wrapper(cls):
        return metaclass(cls.__name__, cls.__bases__, cls.__dict__.copy())
    return wrapper


class _SwigNonDynamicMeta(type):
    """Meta class to enforce nondynamic attributes (no new attributes) for a class"""
    __setattr__ = _swig_setattr_nondynamic_class_variable(type.__setattr__)


SUPPRESS_KIP_CORE = _kipservice.SUPPRESS_KIP_CORE
SUPPRESS_KIP_DAEMON = _kipservice.SUPPRESS_KIP_DAEMON
KIPSUM_MAXSIZE = _kipservice.KIPSUM_MAXSIZE
KIP_KEYUSAGE_USERDATA = _kipservice.KIP_KEYUSAGE_USERDATA
KIP_KEYUSAGE_MAPPING = _kipservice.KIP_KEYUSAGE_MAPPING
KIP_KEYUSAGE_INTEGRITY = _kipservice.KIP_KEYUSAGE_INTEGRITY
KIP_KEYUSAGE_SERVICE = _kipservice.KIP_KEYUSAGE_SERVICE

def kipservice_init(opt_rootkey, opt_hosts):
    return _kipservice.kipservice_init(opt_rootkey, opt_hosts)

def kipservice_fini():
    return _kipservice.kipservice_fini()

def kipservice_start(ctx):
    return _kipservice.kipservice_start(ctx)

def kipservice_stop(ctx):
    return _kipservice.kipservice_stop(ctx)

def kipservice_restart(ctx):
    return _kipservice.kipservice_restart(ctx)

def kipservice_set_client_realm(ctx, crealm):
    return _kipservice.kipservice_set_client_realm(ctx, crealm)

def kipservice_set_clientuser_login(ctx, user_login):
    return _kipservice.kipservice_set_clientuser_login(ctx, user_login)

def kipservice_set_clientuser_acl(ctx, user_acl):
    return _kipservice.kipservice_set_clientuser_acl(ctx, user_acl)

def kipservice_realm(svcmud, realm):
    return _kipservice.kipservice_realm(svcmud, realm)

def kipservice_tomap(ctx, domain, keytohide, blacklist_users, whitelist_users, from_or_0, till_or_0, out_svckeymud, out_svckeymudlen):
    return _kipservice.kipservice_tomap(ctx, domain, keytohide, blacklist_users, whitelist_users, from_or_0, till_or_0, out_svckeymud, out_svckeymudlen)

def kipservice_frommap(ctx, svckeymud, out_mappedkeys):
    return _kipservice.kipservice_frommap(ctx, svckeymud, out_mappedkeys)

def kipservice_sign(ctx, keyid, sumslen, sums, metalen, opt_meta, out_mud, out_mudlen):
    return _kipservice.kipservice_sign(ctx, keyid, sumslen, sums, metalen, opt_meta, out_mud, out_mudlen)

def kipservice_verify(ctx, keyid, sumslen, sums, mud, mudlen, out_metalen, out_opt_meta):
    return _kipservice.kipservice_verify(ctx, keyid, sumslen, sums, mud, mudlen, out_metalen, out_opt_meta)

def qsasl_init(_opt_driver, appname):
    return _kipservice.qsasl_init(_opt_driver, appname)

def qsasl_fini():
    return _kipservice.qsasl_fini()

