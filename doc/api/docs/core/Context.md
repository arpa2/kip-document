# KIP core concept `Context`

> *Before using KIP, establish a referential/managing context.*

To use KIP, first open a `Context`.  The `kip.arpa2.Context` class
is the simplest version, requiring only `libkip.so` but not
`libkipservice.so` or `libkipdaemon.so`; those are however required
for `kip.arpa2.service.ServiceContext` and
`kip.arpa2.daemon.DaemonContext`.

Through the `Context`, it is possible to get handles for `Key` and
`Sum` objects, mix and mingle their entropy and so on.

**Interface.**
Calls available on KIP Context objects are:

  * `Context()` creates a KIP Context,  the starting point for using KIP.
    Note that garbage collection will clean up the context, and
    at that time any keys are overwritten with zeroes before the
    memory is returned to the memory pool.

  * `random(lenght)` produce random bytes of the desired length.

  * `key_generate(algid,keynr)` generates a new symmetric key for the
    given algorithm identifier and key number.

  * `key_fromnumber(keynr)` maps an incomplete key number as it
    travels over the wire to a `Key` object (with its locally unique
    extension).  This operation outputs a list of `Key` objects,
    each of which are candidates for what the key number may mean.

  * `key_mixer(key_salt_pairset)` develops a set of `(key,salt)` pairs
    into a new key.  This is a generalised form of the mechanism
    defined in RFC 6113, where two keys are mixed.  It uses sets to
    avoid multiple occurrences of the same `(key,salt)` which would
    cancel out.

  * `key_frommap(bytes_keymud)` takes a string of `keymud` bytes to
    produce the key concealed inside it, and it returns a
    corresponding `Key` object.  This is the inverse operation of
    `tomap()` defined on `Key`.

  * `key_fromservice(bytes_svckeymud)` takes a string of `svckeymud`
    bytes to produce the keys concealed inside it and return the
    corresponding `Key` objects as a list.

  * `key_fromkeytab(keynr,algid,domain,service=None,hostname=None,ktname=None)`
    looks up a key in a MIT Kerberos5 key table, based on the keynr/alg and
    the domain and, if non-standard, the hostname.  The ktname can be set to
    a specific key table if the usual search mechanisms are considered to
    not work for the application at hand.

  * `claim_keynr()` claims a key number, even before having a keyid setup
    for it.  A new key number is returned, and an empty list of keyids is
    setup as a form of claim.

  * `have_kipservice(stop=False)` ensures that the KIP Service has started
    (or stopped).  This is called from various places that demand KIP Service
    (or not, if they ceased their demand).

  * `have_kipservice_variables()` loads the values `KIP_REALM`,
    `KIPSERVICE_CLIENT_REALM`, `KIPSERVICE_CLIENTUSER_LOGIN` and
    `KIPSERVICE_CLIENTUSER_ACL`; substitute defaults in case of doubt.
