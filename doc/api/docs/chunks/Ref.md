# Referential integrity in `Ref` chunks

> *References include data from another source, possibly under the
> protection of a (secure) hash.*

The `Ref` concept is crucial in the separation between signing a
KIP Document without having loaded the referenced content, yet to
maintain a chain of trust.

The vital choice is that the reference is signed, but never the
referenced content.  If (and only if) the `Ref` involves a secure
hash can the referenced document be securely inserted.

The responsibility for this secure referencing and checking it
rests with the application.  To be totally clear: the KIP Document
class does not validate that a secure reference is indeed secure.

**Attributes.**
The content of a `Ref` object is the `ref` and a `subdoc`.  It is
assumed that the `subdoc` is another KIP Document, but the only
required on `ref` is that it be a dictionary.

**Storage.**
Only the `ref` is stored; the `subdoc` is considered a cached
version of the referenced document and so it may be removed at
anytime.

**Code.**
Create a new `Ref` chunk (possibly to append to the KIP Document)
using the creator,

```python
refdata = {
	'url': 'https://example.com/hiero',
	'sha256': b'\xb3\x97\x23...\xd71\x44\xbe',
}

refobj = Ref (refdata)

doc.append (refobj)
```

After downloading the document, possible

```python
subdoc = Document ()
...
refobj.set_subdoc (subdoc)
...
if refobj.got_subdoc ():
	... refobj.get_subdoc () ...
```

**CBOR.**
The `subdoc` is not stored, but the `ref` dictionary is.  The
dictionary is flatted, CBOR style, and counted as one sub-object
in an array for `Ref` that starts with its tag, 2.

```json
[ 2, {...url...sha256...} ]
```
