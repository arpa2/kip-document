# CBOR Chunks

> *We selected CBOR for the KIP Document format.  This is one format out of many,
> but it is compact, easy to process and concisely specified.  It is not that
> different from ASN.1, except that length fields range over the number of
> encompassed objects, not their byte size, which makes editing much easier.*

CBOR is defined in [RFC 8949](https://datatracker.ietf.org/doc/html/rfc8949).

Apart from the header, we use simple arrays that start with a (small) numeric
tag to distinguish the various types of KIP Document chunks.  This prefix can
usually be stored in 2 bytes (of to head an array of a small size, and one
for the small integer).
