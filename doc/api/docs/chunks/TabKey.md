# Table Keys in `TabKey` chunks

> *Table Keys are symmetric keys, taken from Kerberos keytabs.*

This is a simple mechanism for bootstrapping with local keys.
Whether it is suitable for production environments is questionable.

Table keys allow loading keys into a KIP Document by reference.
They might be useful to edit a document that is stored in an insecure
place, without making it visible or without permitting undetected change.

The tables used are Kerberos5 keytabs, as used in MIT Kerberos.  They
have no encryption on the keys, so their security depends solely on
operating system access control.  In such keytabs, the keys are indexed
by their hostname, domain, key number and standardised algorithm id.

These may be difficult to protect on client systems, and the
general advise is to only use them on servers.  Having said
that, it may beat the lack of entropy of a password, but the
big problem is that keytabs do not protect the key at all.
The only keys that can be obtained via `TabKey` are named as
`kip/host@DOMAIN`, to protect users from leaking other patterns.

**Code.**
To load a key from a table, create a `TabKey` chunk and insert it
into the document before the key is first used:

```python
tk = TabKey ('host.example.com', 'EXAMPLE.COM', 123, 830)
```

**CBOR.**
The format of a `TabKey` key introduction in the CBOR file is an
array with tag 6, the hostname, the domain/realm, the key number
and algid.

```json
[ 6, "host.example.com", "EXAMPLE.COM", 123, 830 ]
