# Filenames for KIP Document

> *Files holding a KIP Document can be recognised by their name.
> And sometimes we use a directory.*

We suggest using `.kip` as the filename extension for files with
KIP Document content.  This is not a common extension and so it should
not cause confusion.

The [magic header](../header) allows additional confirmation of the content
of the file.

We may expand a `.kip` file into a directory of the same name.  This can be
useful to cache `Ref`erenced documents, as well as to facilitate (re)hashing
their contents.

