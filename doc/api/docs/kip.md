# Keyful Identity Protocol (KIP)

> *Sending documents with encryption is difficult.  Not all recipients hold
> keys, or they may not have passed them over to a sender, or the validation
> that the key belongs to the intended user may be difficult or incomplete.
> KIP lets people decrypt after authentication only, which means that it is
> much easier to send encrypted documents anywhere.*

  * KIP has a [project page(https://gitlab.com/arpa2/kip)] and [online documentation](http://kip.arpa2.net/)
  * KIP is the general core library underneath KIP Document
  * KIP Document has a [project page](https://gitlab.com/arpa2/kip-document) and [online documentation](https://kip-document.arpa2.net/)

The idea of KIP is that users can authenticate to a simple, mindless
KIP Service which runs for them under their domain name.  This KIP Service
will grant encryption requests to anyone, but only grant decryption to
people (or domains) who are on an Access Control List.  There are both
black lists and white lists in this ACL.  Through login, a user can show
their identity, be granted or rejected according to the ACL, and be sent
a decryption key.  The actual information does not pass through this
KIP Service.

Likewise, the KIP Service will support signature validation by anyone.
That is, it will indicate that a document's hash was made on the server
and set to a given value.  Once again the document itself does not pass
through the server.  To create a signature, a user must login to the
KIP Service and present the document hash to be turned into a signature.

An advanced feature is the use of encryption on keys.  This is called a
key mapping, because it uses readily available encryption key and delivers
new ones.  Not everyone will be able to use the same key mappings, and
so the various keys make it possible to conceal certain data from certain
users, in any way desired.  This variation is emobied by KIP Documents.

**KIP core concepts.**
Lower-level concepts from the KIP core library as well as KIP Service
are available in the `arpa2.kip.api` module, and exhibited through the
following classes:

  * [`Context`](../core/Context)
  * [`Key`](../core/Key)
  * [`Sum`](../core/Sum)

Various forms of xxxMud originate from this layer.  Not all of these have
been wrapped, to avoid taking away the flexibility of full-blown KIP.
