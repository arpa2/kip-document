# Programming concept: `BaseInteractor`, `VerboseInteractor`

> *Classes that help to gather sucesses and failures.*

An object of one of these, or a local flavour for the `BaseInteractor`, is meant
to connect programs to the internal processing of a KIP Document.

Please see the inline documentation of `BaseInteractor` for details.  General
topics of concern are:

  * Passphrase collection
  * Key tab filename configuration
  * Reports on signature creation, validation
  * Reports on trusted content
  * Reports on muddled content creation and and making Mud readable

The `VerboseInteractor` is just one that mindlessly blurts out all that it gets.

