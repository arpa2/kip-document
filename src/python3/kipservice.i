%module kipservice

%include "stdint.i"
%include "typemaps.i"
// %include "cpointer.i"

%{

#define SUPPRESS_KIP_CORE    1
#define SUPPRESS_KIP_DAEMON  1
#undef  SUPPRESS_KIP_SERVICE
#include <arpa2/kip.h>

bool qsasl_init (const char *_opt_driver, const char *appname);
void qsasl_fini (void);

%}


/* Construct new_T() and T_value() functions -- as well as refcount support */
// %pointer_functions (kipt_ctx,ctx);
// %pointer_functions (kipt_alg,alg);
// %pointer_functions (kipt_keynr,keynr);
// %pointer_functions (kipt_keyid,keyid);
// %pointer_functions (uint32_t,length);
// %pointer_functions (PyObject,list);


%{

struct svckeyparams {
	kipt_keyid *mappedkeys;
	uint32_t  mappedkeyslen;
	PyObject *list;
};

%}


/* Typemap for (buf,len) pair; must not be None; readonly is ok */
%typemap(in)
	(uint8_t *svckeymud,uint32_t svckeymudlen) {
                printf ("Type mapping svckeymud/svckeymudlen\n");
                Py_buffer view;
                PyObject_GetBuffer ($input, &view, PyBUF_ANY_CONTIGUOUS);
                $1 = (uint8_t *) view.buf;
                $2 = (uint32_t ) view.len;
                printf ("Type mapped  svckeymud/svckeymudlen\n");
        }


/* Typemap for an input/output list of keyid_t */
%typemap(in)
	(kipt_keyid **out_mappedkeys,uint32_t *out_mappedkeyslen) {
		struct svckeyparams *p = malloc (sizeof (struct svckeyparams));
		p->mappedkeys = NULL;
		p->mappedkeyslen = 0;
		p->list = $input;
		$1 = &p->mappedkeys;
		$2 = &p->mappedkeyslen;
		// Py_INCREF (p->list);
	}
//
%typemap(freearg)
	(kipt_keyid **out_mappedkeys,uint32_t *out_mappedkeyslen) {
		struct svckeyparams *p = (struct svckeyparams *) $1;
		for (uint32_t i = 0; i < p->mappedkeyslen; i++) {
printf ("Mapping entry %d / %d valued %d\n", (int) i, (int) p->mappedkeyslen, (int) p->mappedkeys [i]);
			PyObject *newkey = PyLong_FromLong ((long) p->mappedkeys [i]);
			PyList_Append (p->list, newkey);
printf ("Mapped entry %d / %d\n", (int) i, (int) p->mappedkeyslen);
		}
printf ("Returning the mapped keys\n");
		// PyObject *output = p->list;
		free (p);
		// $result = output;
	}


#define SUPPRESS_KIP_CORE    1
#define SUPPRESS_KIP_DAEMON  1
#undef  SUPPRESS_KIP_SERVICE
%include <arpa2/kip.h>

bool qsasl_init (const char *_opt_driver, const char *appname);
void qsasl_fini (void);

