#
# File I/O for arpa2.kip
#
# From: Rick van Rein <rick@openfortress.nl>
#


import re
import os.path

from arpa2.kip import io


regexes = {
	'stdio': re.compile ('^[.]$'),
	'url':   re.compile ('^file://(?:localhost)?(/.*)$'),
	'path':  re.compile ('^(/[^/.].*)$'),
}


class FileSource (io.Source):
	"""FileSource provides KIP I/O for local files, in this case
	   for reading.  Both the URI form and an absolute path are
	   considered exact matches.
	"""

	exact_regexes = regexes

	def __init__ (self, exact_match=None):
		assert exact_match is not None
		(_form,mtch) = exact_match
		self.path = mtch.group (1)
		self.mime = None

	def get_metadata (self):
		return {
			'mediaType': self.get_mimetype ()
		}

	def get_mimetype (self):
		if self.path == '.':
			self.mime = 'text/plain'
		elif self.mime is None:
			try:
				cmd = 'file --mime-type "%s"' % self.path
				self.mime = os.popen (cmd).read ().rstrip ().split ()[-1]
			except:
				self.mime = 'application/octet-stream'
		return self.mime

	def get_namehint (self):
		if self.path == '.':
			return None
		elif not os.path.exists (self.path):
			return None
		elif os.path.isdir (self.path):
			return None
		else:
			return os.path.basename (self.path)

	def open (self):
		"""Open the file and return it, or raise an exception
		   on failure.
		"""
		if self.path == '.':
			return sys.stdin
		if self.get_mimetype () [:5] == 'text/':
			#TODO# wrapper?
			of = 'rb'
		else:
			of = 'rb'
		return open (self.path, of)


class FileTarget (io.Target):
	"""FileTarget provides KIP I/O for local files, in this case
	   for writing.  Both the URI form and an absolute path are
	   considered exact matches.
	"""

	exact_regexes = regexes

	def __init__ (self, exact_match=None):
		assert exact_match is not None
		(_form,mtch) = exact_match
		self.path = mtch.group (1)

	def open (self, origin):
		"""Open the file, given that the presented io.Source
		   object represents the matching io.Source document.
		   Return a file-like object or raise an exception
		   on failure.
		"""
		assert isinstance (origin, io.Source)
		if self.path == '.':
			return sys.stdout
		if os.path.isdir (self.path):
			namehint = origin.get_namehint ()
			if namehint is None:
				raise Exception ('Directory path means that a name_hint is needed')
			if os.path.sep in namehint or namehint [:1] == '.':
				raise Exception ('Directory needs name_hint, but looks devious')
			self.path = os.path.join (self.path, namehint)
		if origin.get_mimetype () [:5] == 'text/':
			#TODO# wrapper?
			of = 'wb'
		else:
			of = 'wb'
		return open (self.path, of)

