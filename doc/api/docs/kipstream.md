# KIP Streams

> *Nothing about KIP Documents makes them batch-mode objects.
> You can stream contents with security if you like.*

The chunks of a KIP Document are simply placed in sequence, and the computation
of signatures and encryption works in the reading direction.  This may also be
the time direction.  It is possible to apply this on streaming media, for
instance.  As always with KIP, there is no need to hold on to key material before
this can start; each recipient is responsible for obtaining their own key material.

One note worth making is that a signature cannot be validated on parts of a
streaming document.  Only when a signature is hit, can the preceding part be
validated.  When playback commences before this point, it is vital that this
is made aware to the receivers.  It is possible to sign the data at places
that make most sense to the application and its framing properties, of course.

The flexible play with keys, notably through key maps, can be helpful in doing
clever things, such as splitting off new keys from time to time, in a rolling
motion (think of Signal's key ratchet concept).  During setup, such as with
SIP, the key exchange can be initiated and the keys can then be used on RTP
elements, where the frame numbering in RTP may be used in the mix.

Specifically note that, although CBOR has an open-ended form for its data
structures, such is not required in a normal use of KIP Document, just to
achieve KIP Streams.  This is one reason why the definition of a KIP Document
is just a _sequence of chunks_ (after a header).

**Default Profile.**
Every application may set its own standards, but the following rules should
make sense as defaults.  Aberrations should be well-documented to avoid
program confusion or conflicts around compatibility.

  * Do not include the [header](../header) but leave it to the surrounding
    protocol to negotiate a [MIME-type](../mime) and start/end markers.
    This is specifically supported in SIP and SAP through SDP attachments
    and their RTP streams.  It is even possible to use RTSP to issue
    commands like PLAY, PAUSE and STOP.  This is offers more subtleth than
    a document header could possibly express.

  * Do not break up CBOR chunks over transmission frames, except when these
    frames are used with reassembly.  This aids straightforward CBOR parsing
    with the simplest, whole-chunk-driven CBOR parsers.  If you must break
    up CBOR chunks, then consider doing it through the logic of the
    application, so as to break up [content chunks](../chunks/Content).
    By way of hint: SCTP is eminently suitable as a transport, and it is
    supportive of out-of-order delivery where supported, and in-order where
    needed.  And it can run over UDP, so it works everywhere.

  * Use frame transport properties for a clear discipline on whether chunks
    are missing, and whether delivery is in-order.  Be clear on the meaning
    of drop-outs, drop-ins and so on, in the semantics of your KIP Document.
    In general, the format is fragile when data is dropped, because keys may
    disappear and disable later decoding, and signature validations are also
    like to fail.  At the same time, KIP Document is not fragile towards
    such problems, but it may impair user experience.  Whether that is
    problematic for a KIP Stream is up to the application.

  * Be clear on the options for mixing KIP Streams into one KIP Stream, such
    as might come up during conference calls, group games and so on.  Having
    no split-up chunks is helpful, and being able to mix KIP Documents even
    with overlapping key identifiers is also useful, but it may be highly
    confusing to the application when the [content chunks](../Content) is
    not presented as one steady flow.  By default, mixing is permitted, but
    the reqult might be like a terminal on which several loud programs are
    all blurting at the same time.  The transport frames however, may carry
    information to keep the KIP Streams apart, even after mixing.
