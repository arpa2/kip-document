# Header of a KIP Document

> *KIP Documents can be recognised in a few manners, among which
> a magic code at the start.*

The header is an optional element, most notably inserted at the start of
a file, but completely optional in places where other elements of the
context, such as [filename extension](../ext) or [MIME-type](../mime)
give enough clarity.

**Structure.**
The start of a KIP Document is an ASCII string of 18 characters, plus a
trailing newline, coming to a total of 19 bytes:

  * The start is literally `ARPA2KIP`
  * Following that is the `YYYYMM` formated specification version time
  * The following 4 characters use base64 notation for 4x6 = 24 flags,
    MSB sextet first and LSB sextet last.
  * The trailing newline byte `'\n'` or U+000a.  No carriage return,
    no ASCII NUL character.

**Skipping the header.**
This header can be parsed with a normal CBOR parser, which sees it as:

 1. A byte string of length one, `b'R'`
 2. A byte string of length 15, `b'A2KIP202003===='` when the specification
    date is 2020-03 and the flags are zero.
 3. The letters `A` and `P` dropped out; their highest 3 bits encode for
    a byte string and their lower 5 bits encode a length of 1 and 16.

It is up to you whether to scan for the literal start bytes and parse
the rest, or to use the CBOR parser that is already needed for the job.
In the latter case, be sure to validate the string lenghts.

The flags are information, they hint at intended patterns.  Enforcement
is always a choice of the operator working on the KIP Document.

**QR-code and data: URIs.**
It is not a good idea to include the header at the beginning of a QR-code,
or more generally said, in the beginning of a `data:` URI, because there
already is a [MIME-type](../mime) and the available space is often tight.

**KIP Streams.**
It is possible, though not advised, to inject a header into a KIP Stream.
Normally, the surrounding messaging for setting up the KIP Stream should
suffice to introduce the [MIME-type](../mime) for the stream.  The one
reason for the header could be to synchronise or restart a KIP Stream.

When the semantics of the KIP Stream support mixing of content, then it
may even happen that a later addition to a collaborating group injects
another header, all that is to be considered harmless.  One distinction
that is vital in KIP Streams is that the two CBOR chunks in the header
are passed in immediate succession in the same transmission frame.
