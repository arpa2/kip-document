#
# Generic I/O framework for arpa2.kip
#
# From: Rick van Rein <rick@openfortress.nl>
#


class Connector:
	"""Connector holds the code and abstractions shared by the
	   Source and Target classes in the arpa2.kip.io package.
	   Do not inherit directly from this class, but use its
	   code and definitions through Source or Target.
	"""

	"""Keyed exact matching regexes for the URIs.  Subclasses
	   should override this value to elicit constructive behaviour
	   from the class method from_exact_match().  The key of the
	   regex is used during instance creation, as is the match
	   object produced, together in a a (key,match) pair to the
	   exact_match parameter to the subclass's __init__().
	"""
	exact_regexes = { }

	@classmethod
	def from_exact_match (cls, uri):
		"""Test for an exact match with the URI and, if
		   lucky, return an instance of the (sub)class
		   ready to go.  On failure, return None.
		   
		   Subclasses set a list of re.compile() outputs
		   to match expressions in cls.exact_regexes.
		   The generic code is present, but runs over an
		   empty default list in Connector.exact_regexes.
		"""
		assert issubclass (cls, (Source,Target))
		for (i,rex) in cls.exact_regexes.items ():
			m = rex.match (uri)
			if m is not None:
				return cls (exact_match=(i,m))
		return None

	@classmethod
	def from_interpretation (cls, uri):
		"""Test for a match after interpreting the URI,
		   and return the best match with its probability
		   of being right.  The probability is a figure
		   that helps to sort between various possible
		   interpretations.  The user may override the
		   order in which it is done, but otherwise the
		   probability is leading.  Return either a pair
		   (probability,instance) or failure (0.0,None).
		   
		   Subclasses may override this method if they
		   desire to facilitate this.  The abstract
		   class just returns failure value (0.0,None).
		"""
		return (0.0,None)


class Source (Connector):
	"""arpa2.kip.io.Source classes can be used as entry_point
	   for "arpa2.kip.io.sources" so they are made available
	   to users (who may subsequently configure them).
	   
	   For every source URI in commands like "kip up", "kip down" 
	   and so on, a configured sequence of these classes is
	   tried.  First, an exact match is tried on all configured
	   plugins; when all fail, then more liberal matching is tried,
	   for which they will be sorted by a score.  This allows both
	   easy-intuitive uses and accurate, well-established patterns.
	   
	   To read data, a connection is made that should return a
	   file-like object, possibly with metadata.  Connection failure
	   causes the next pattern to be tried, but an exact match that
	   fails to connect will never fall back to the more liberal
	   matches.
	"""

	def get_metadata (self):
		"""Get a dict with the metadata for this object.
		   Keys are LDAP attribute-type styled, and include
		   mediaType.
		   
		   Subclasses must implement this method.
		"""
		raise NotImplementedError ('arpa2.kip.io.Source:get_metadata()')

	def get_mediatype (self):
		"""Get a str with the mediaType for this object.
		   
		   Subclasses must implement this method.
		"""
		raise NotImplementedError ('arpa2.kip.io.Source:get_mediatype()')

	def get_namehint (self):
		"""Get a name hint (or None) to use in the target
		   if it happens to be a directory.
		   
		   Subclasses may implement this method.
		"""
		return None

	def open (self):
		"""Open the file and return it, or raise an exception
		   on failure.
		   
		   Subclasses must implement this method.
		"""
		raise NotImplementedError ('arpa2.kip.io.Source:open()')


class Target (Connector):
	"""arpa2.kip.io.Target classes can be used as entry_point
	   for "arpa2.kip.io.targets" so they are made available
	   to users (who may subsequently configure them).
	   
	   For every target URI in commands like "kip up", "kip down" 
	   and so on, a configured sequence of these classes is
	   tried.  First, an exact match is tried on all configured
	   plugins; when all fail, then more liberal matching is tried,
	   for which they will be sorted by a score.  This allows both
	   easy-intuitive uses and accurate, well-established patterns.
	   
	   To write data, a connection is made, possibly with metadata,
	   that should return a file-like object.  Connection failure
	   causes the next pattern to be tried, but an exact match that
	   fails to connect will never fall back to the more liberal
	   matches.
	"""

	def open (self, origin):
		"""Open the file and return it, or raise an exception
		   on failure.  The origin is an io.Source instance
		   that may be queried for additional information.
		   
		   Subclasses must implement this method.
		"""
		raise NotImplementedError ('arpa2.kip.io.Target:open()')

