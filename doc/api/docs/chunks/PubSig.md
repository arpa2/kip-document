# Public Signature completion in `PubSig` chunks

> *Public Signatures are made with a public key, and not necessarily
> related to PubKey chunks.  The start of the signed chunks is marked
> by a PubDig chunk.*
