# KIP core concept `Key`

> *Key objects are vital to all cryptographic operations.*

Keys define operations that they can perform on data passing through.
This is mostly encryption and decryption, but also passing them
through mappings to allow key derivation schemes.

Every `Key` is also a `Sum`, or checksum, and it will track all bytes
passing through it as such; this is why `Key` is a subclass of `Sum`.
It is quite possible to create distinct `Sum` objects that are not
themselves `Key` objects but rather associated to other `Key` objects,
but often the one that comes with each `Key` will do nicely.

**Interface.**
The following operations are available on `Key` objects:

  * `Key(context,keyid)` creates a new `Key` object, referencing the
    `Context` and identified by the `keyid` as given.

  * `keyid()` returns the unique, numeric key identity.  Contrast that
    with the `keynr()` method that returns a value that is not
    necessarily unique.  Being able to deal with external key numbers
    that may or may not be unique helps simplifying the processing of
    KIP Documents created by collaborating authors, especially because
    not everyone needs to have access too all keys in the KIP Document.

  * `keynr()` returns the not-necessarily unique, wire-sized key
    number.  Since it is deliberately not a KIP Document requirement
    that its keys are uniquely numbered, we use this notion of a key
    number for the KIP Document format, but pair it with an internally
    unique `keyid()` system that helps us to multiple `Key` objects
    that serve as candidates for each key number.  Since key ids are
    not transmitted in a KIP Document, their numbers may vary between
    local sites.

  * `algorithm()` retrieves the algorithm identifier for this `Key`.

  * `kip_up(content)` applies encryption and MAC/hashing to the bytes
    of content and returns muddled content.  This operation does part
    of the larger operations performed on a KIP Document.

  * `kip_down(mud)` applies decryption and MAC/hash checking to the
    muddled bytes and returns the content.  This operation does part
    of the larger operations performed on a KIP Document.

  * `usermud(salt,mudlen)` applies the key to the salt to derive
    user-mud of the requested mudlen, which is exported to the
    user/application and may be used as key material of similar
    entropy as the salt.  Unlike an actual key however, this mud
    is not maintained by KIP, but surrendered to the application/user.

  * `mixer(salt)` develops this key into another, by incorporating salt.
    This is a limited form of the mixed defined on the
    [`Context`](../Context).  The salt may be public material, but
    should have enough entropy to reach a sufficiently different `Key`.

  * `tomap(*mapkeys)` maps this key through a list of other keys.  The
    mirrorring operation is `frommap()`, defined on the
    [`Context`](../Context) because no `Key` exists yet when it is called.

  * `sum_start()` constructs  a new Sum object, based on this `Key` but
    not updated by its `kip_up()` and `kip_down()` operations.  The idea
    is that this `Key` will be used for signatures and their verification.
    The `Key` also helps to define a hashing algorithm.  Sum IDs are a
    superset of Key IDs, but it is usually called by the most-occurring
    type space of the Key ID.
