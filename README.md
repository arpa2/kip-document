# KIP Document

> *KIP Document allows document editing with signatures and encryption for
> multiple editors.  Signatures can be retained as having been made by
> prior signers.  Encryption can be used to hide certain content from
> (all but) certain editors, or readers.*

Underneath KIP Document, there is a flexible security module named
[KIP](https://kip-document.arpa2.net/kip).  This library and daemon make it
possible to handle user identities, not keys or certificates.  The link
between the two is made with a simple KIP daemon that runs under a domain,
and its task is to translate identity into an ability to decrypt data
and to sign data.  Traditional crypto required local use of a key, this
new approach requires authentication to an online service that runs
under the domain part of your identity.

KIP Document builds on this, and constructs documents (of any kind) from
chunks.  There are different
[chunk types](https://kip-document.arpa2.net/chunks/Chunk)
to separately hold
[user data](https://kip-document.arpa2.net/chunks/Content) interspersed with
[key introductions](https://kip-document.arpa2.net/chunks/KeyIntro) or
[signature validators](https://kip-document.arpa2.net/chunks/Validator).
An overview of the entire system can be found in the [API documentation](https://kip-document.arpa2.net/).


## General Use

The use of a KIP Document works very simply:

  * `kip_up()` cranks up security.  It makes/refreshes signatures and encrypts
    content, inasfar as possible with locally available keys.  It will not fail,
    but rather compensate, for missing keys held by co-editors.

  * `kip_down()` removes security barriers.  It validates signatures and
    decrypts content, inasfar as possible with locally available keys.  It will
    not fail, but rather commpensate, for missing keys held by co-editors.

Details exist, of course, and are detailed in the [API documentation](https://kip-document.arpa2.net/).


## KIP Document in any Programming Language

The KIP library and daemon are C programs.  Using SWIG wrappers, the KIP API
was translated to Python3, the layer of the KIP Document layer.  The API for
KIP Document was built on top of these primitives.  The same
concepts can follow this same approach, and be ported to
Java, Lua, NodeJS, Perl, PHP, Ruby, Rust, ...


## Setup with Python3

```
python3 src/setup.py build
python3 src/setup.py install
```

