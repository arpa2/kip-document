#
# Python API for KIP Core, as described in doc/PythonAPI.md
#
# From: Rick van Rein <rick@openfortress.nl>
#


# Basic import of the KIP Core wrapper
#
import _kipcore as _c


# Try to import KIP Service API, testing if it is locally available
#
try:
	# import arpa2.kip.kipservice as _s
	import _kipservice as _s
	loaded_service = True
	_s.qsasl_init (None, "Hosted_Identity")
	_s.kipservice_init (None, None)
except ImportError:
	loaded_service = False


# Try to import KIP Daemon API, testing if it is locally available
#
try:
	# import arpa2.kip.kipdaemon as _d
	import _kipdaemon as _d
	loaded_daemon = True
except ImportError:
	loaded_daemon = False



# arpa2.kip.Sum class definition
#
class Sum:
	"""The Sum class collects checksum information, and can be updated
	   with new data at any time.  It offers a few advanced facilities,
	   such as forking or cloning the hash and marking syntactical spots
	   of interest that should not cause security problems as a result
	   of invisible binary boundaries.
	   
	   The need for a separate checksum is not just because of the more
	   powerful functionality than the MAC included in kip_up(), but
	   there are strong security reasons, such as the number of bits
	   involved, the data spanned and the longevity of the data.  Try
	   to think of the MAC as a short-lived, message-specific protection
	   against _accidental_ changes, whereas a Sum protects against
	   _deliberate_ assaults on your security.  And yes, it takes some
	   determination to mount an attack on a MAC and if you test it a
	   few times by hand you should see you bit changes or byte removals
	   trigger alerts quite consistently; but if you really wanted to
	   put in the effort and compute power you would probably find a
	   way to trick the MAC.  Not so with the Sum.  So when you need
	   to protect anything but messages in transit as KIP does for its
	   internal purposes, use a Sum.  Really, don't be shy and just
	   always use a Sum.
	"""

	def __init__ (self, ctx, kid):
		"""Initialise a new Sum object, to manage the checksum with
		   the support of the identified key.  Note that every Key
		   already has a Sum builtin by being a subclass hereof.
		   Note that "kid" is actually a sum-or-key-id, which is a
		   larger set of values than just key-id.
		"""
		"TODO:SPLIT_keyid_AND_sumid"
		self.ctx = ctx
		self.kid = kid

	def sumid (self):
		"""Return the Sum ID for this Sum.  Note that the Sum ID
		   type is a superset of the Key ID set, so all Key IDs
		   also count as Sum IDs.  This routine will return the
		   same value as keyid() if this Sum also happens to be
		   a Key and therefore supports the keyid() call too.
		"""
		return self.kid

	def append (self, bytes_data):
		"""Append data bytes to this Sum object or, if this object
		   happens to also be a Key, insert it between data that
		   passes through kip_up() and kip_down() calls.
		"""
		_c.kipsum_append (self.ctx, self.kid, bytes_data)

	def restart (self):
		"""Restart the Sum object in a state that has not seen any
		   data passing in yet.  This works for Key objects too,
		   and then forgets all kip_up() and kip_down() data too.
		"""
		_c.kipsum_restart (self.ctx, self.kid)

	def fork (self):
		"""Fork the Sum in its current state, and allow the old and
		   new to continue in different or same directions without
		   any further relation; do however share the history up to
		   the moment of this fork() call.  The forked Sum will be
		   tied to the same Key, but it will not be updated by its
		   kip_up() and kip_down() operations, while that aspect of
		   the original Sum continues to exist or not, as before.
		"""
		sumidp = _c.new_keyid ()
		_c.kipsum_fork (self.ctx, self.kid, sumidp)
		sumid = _c.keyid_value (sumidp)
		return sumid

	def mark (self, typing=None):
		"""Insert a separation marker into the hash flow.  This is
		   a new idea.  It allows clear separation between bits
		   that must never been seen to connect, and follows the
		   basic philosophy that hash input is secure if it could
		   be parsed back.  In this case, markers are inserted in
		   the input flow, as well as an extra one at the end, in
		   a reproducable manner that requires no escaping and no
		   special characters.  To yield the best possible result,
		   you could add a textual hint about the marker point,
		   such as a syntactical choice made on account of prior
		   and/or following data.  Providing no typing differs
		   from providing any string value, even an empty string.
		   You are welcome to use international type; we will map
		   it in a repeatable manner to a sequence of bytes.  Note
		   that it is text, so there are no intermediate 0x00 bytes.
		   This imposes no restrictions on the other bytes, though.
		"""
		if type (typing) == str:
			typing = bytes (typing, 'utf-8')
		_c.kipsum_mark (self.ctx, self.kid, typing)

	def merge (self, *addends):
		"""Merge other sums' current/intermediate hash values into
		   this one and then continue.  This allows the merging of
		   data flows.  The addends are processed in the order in
		   which they appear as arguments.
		"""
		for addend in addends:
			_c.kipsum_merge (self.ctx, self.kid, addend.sumid ())

	def sign (self):
		"""Produce signature mud for the current Sum state, where
		   the bound Key is used to produce the signature.  Note
		   that every Sum has an associated Key, not just the ones
		   that happen to play the role of Key through subclassing.
		   Operations like sum_start() and fork() always share the
		   Key ID into the newly created Sum object.
		"""
		#TODO# This is a _terrible_ size exchange, and too much even for SWIG
		sigmudlenp = _c.new_length ()
		assert not _c.kipsum_sign (self.ctx, self.kid, 0, sigmudlenp, None)
		sigmudlen = _c.length_value (sigmudlenp)
		sigmud = bytearray (sigmudlen)
		assert _c.kipsum_sign (self.ctx, self.kid, sigmudlen, sigmudlenp, sigmud)
		return bytes (sigmud)

	def verify (self, bytes_sigmud):
		"""Verify signature mud bytes against the current Sum state, and
		   use the bound Key to verify the signature.  To make this work,
		   the same Key ID must have been associated with this Sum as was
		   shared before the verified sign() call; that is the responsibility
		   for the application programmer.
		"""
		#TODO# Deal with False return value; differentiate errno values
		return _c.kipsum_verify (self.ctx, self.kid, bytes_sigmud)



# arpa2.kip.Key class definition
#
class Key (Sum):
	"""Keys define operations that they can perform on data passing through.
	   This is mostly encryption and decryption, but also passing them
	   through mappings to allow key derivation schemes.
	   
	   Every Key is also a Sum, or checksum, and it will track all bytes
	   passing through it as such; this is why Key is a subclass of Sum.
	   It is quite possible to create distinct Sum objects, but often the
	   one that comes with each Key will do nicely.
	"""

	def __init__ (self, apictx, kid):
		"""Initialise a new Key object, referencing the Context and
		   identified by the KeyID as given.
		"""
		super(Key,self).__init__ (apictx.ctx,kid)
		#super# self.ctx = ctx
		#super# self.kid = kid
		knr = kid & 0xffffffff
		if not knr in apictx.keymap:
			apictx.keymap [knr] = [ self ]
		else:
			apictx.keymap [knr].append (self)

	def keyid (self):
		"""Return the unique, numeric key identity.
		"""
		return self.kid

	def keynr (self):
		"""Return the not-neccessarily unique, wire-sized key number.
		"""
		return self.kid & 0xffffffff

	def algorithm (self):
		"""Retrieve the algorithm for this Key object.
		"""
		algp = _c.new_alg ()
		assert _c.kipkey_algorithm (self.ctx, self.kid, algp)
		alg = _c.alg_value (algp)
		return alg

	def kip_up (self, bytes_data):
		"""Apply encryption and MAC to the data bytes and return
		   the mud that results from "kip up".
		"""
		mudlenp = _c.new_length ()
		assert not _c.kipdata_up (self.ctx, self.kid, bytes_data, None, mudlenp)
		mudlen = _c.length_value (mudlenp)
		print ('mudlen %r :: %r' % (mudlen,type(mudlen)) )
		mud = bytearray (mudlen)
		assert _c.kipdata_up (self.ctx, self.kid, bytes_data, mud, mudlenp)
		mud = bytes (mud)
		return mud

	def kip_down (self, bytes_mud):
		"""Apply decryption and MAC checking to the mud bytes and
		   return the result from "kip down".
		"""
		clearlenp = _c.new_length ()
		clear = bytearray (len (bytes_mud))
		assert _c.kipdata_down (self.ctx, self.kid, bytes_mud, clear, clearlenp)
		clearlen = _c.length_value (clearlenp)
		clear = bytes (clear [:clearlen])
		assert len (clear) == clearlen
		return clear

	def usermud (self, salt, mudlen):
		"""Apply the key to the salt to derive usermud of the requested
		   mudlen, which is exported to the user/application and may
		   be used as key material of similar entropy as the salt.
		   Unlike a actual keys however, this mud is not maintained
		   by KIP, but surrendered to the application/user.
		"""
		mud = bytearray (len (mudlen))
		assert _c.kipdata_usermud (self.ctx, self.kid, salt, mud)
		return bytes (mud)

	def mixer (self, salt):
		"""Develop this key into another, by incorporating the salt.
		   This is a limited form of the mixer defined on the Context.
		   The salt may be public material, but should have enough
		   entropy to reach a sufficiently different Key.
		"""
		kidp = _c.new_keyid ()
		assert _c.kipkey_mixer (self.ctx, [ (self.kid,salt) ], kidp)
		kid = _c.new_keyid_value (kidp)
		print ('DEBUG: Key mixed with ID %x' % (kid,))
		return Key (self.ctx, kid)

	def tomap (self, *mapkeys):
		"""Map this key through a list of other keys.  The mirrorring
		   operation is key_frommap(), defined on the Context because no
		   Key exists when it is called.
		"""
		print ('DEBUG: tomap (%r)' % (mapkeys,))
		assert len (mapkeys) > 0
		keymudlenp = _c.new_length ()
		mapkids = [ mapkey.keyid () for mapkey in mapkeys ]
		assert not _c.kipkey_tomap (self.ctx, self.kid, mapkids, None, keymudlenp)
		keymudlen = _c.length_value (keymudlenp)
		keymud = bytearray (keymudlen)
		assert _c.kipkey_tomap (self.ctx, self.kid, mapkids, keymud, keymudlenp)
		keymud = bytes (keymud)
		return keymud

	def sum_start (self):
		"""Construct a new Sum object, based on this Key but not updated
		   by its kip_up() and kip_down() operations.  The idea is that
		   this Key will be used for signatures and their verification.
		   The Key also helps to define a hashing algorithm.
		   Sum IDs are a superset of Key IDs, but it is usually called by
		   the most-occurring type space of the Key ID.
		"""
		sumidp = _c.new_keyid ()
		_c.kipsum_start (self.ctx, self.kid, sumidp)
		sumid = _c.keyid_value (sumidp)
		return Sum (self.ctx, sumid)


# arpa2.kip.Context class definition
#
class Context:
	"""To use KIP, first open a Context.  The kip.arpa2.Context class is the
	   simplest version, requiring only libkip.so but not libkipservice.so
	   or libkipdaemon.so; those are however required for
	   kip.arpa2.service.ServiceContext and kip.arpa2.daemon.DaemonContext.
	"""

	def __init__ (self):
		"""Initialise a KIP Context, the starting point for using KIP.
		   Note that garbage collection will clean up the context, and
		   at that time any keys are overwritten with zeroes before the
		   memory is returned to the memory pool.
		"""
		ctx = _c.new_ctx ()
		_c.kipctx_open (ctx)
		print ('DEBUG: pre,  ctx = %r :: %r' % (ctx,type(ctx)) )
		self.ctx = _c.ctx_value (ctx)
		print ('DEBUG: post, ctx = %r :: %r' % (self.ctx,type(self.ctx)) )
		self.keymap = { }
		self.on_kipservice = False

	def __del__ (self):
		"""Close down this KIP Context, while erasing keys instead of
		   just freeing them into the Python or OS heap.
		"""
		self.have_kipservice (stop=True)
		_c.kipctx_close (self.ctx)
		print ('DEBUG: closed down KIP Context')

	def random (self, length):
		"""Produce random bytes of the desired length.
		"""
		rnd = bytearray (length)
		assert _c.kipdata_random (self.ctx, rnd)
		rnd = bytes (rnd)
		print ('DEBUG: random #%d is #%d' % (length,len(rnd)))
		return rnd

	def key_generate (self, alg, keynr):
		"""Generate a new symmetric key for the given algorithm
		   and key number.
		"""
		kidp = _c.new_keyid ()
		assert _c.kipkey_generate (self.ctx, alg, keynr, kidp);
		kid = _c.keyid_value (kidp)
		print ('DEBUG: Key generated with ID %x' % (kid,))
		return Key (self, kid)

	def key_fromnumber (self, keynr):
		"""Map an incomplete key number as it travels over the wire
		   to a Key object (with its locally unique extension).
		   This operation outputs a list of Key objects.
		"""
		keynr = keynr & 0xffffffff
		return self.keymap.get (keynr, [ ])

	def key_mixer (self, key_salt_pairset):
		"""Develop a set of (key,salt) pairs into a new key.  This
		   is a generalised form of the mechanism defined in RFC 6113,
		   where two keys are mixed.  It uses sets to avoid multiple
		   occurrences of the same (key,salt) which would cancel out.
		"""
		assert type (key_salt_pairs) == set
		key_salt_pairlist = list (key_salt_pairset)
		kidp = _c.new_keyid ()
		assert _c.kipkey_mixer (self.ctx, key_salt_pairlist, kidp)
		kid = _c.new_keyid_value (kidp)
		print ('DEBUG: Keys mixed with ID %x' % (kid,))
		return Key (self.ctx, kid)

	def key_frommap (self, bytes_keymud):
		"""Given a sequence of keymud bytes, produce the key
		   concealed inside it and return a corresponding Key object.
		   
		   This is the inverse operation of tomap() defined on Key.
		"""
		kidp = _c.new_keyid ()
		assert _c.kipkey_frommap (self.ctx, bytes_keymud, kidp)
		kid = _c.keyid_value (kidp)
		print ('DEBUG: Extracted key id %x from map' % kid)
		return Key (self, kid)

	def key_fromservice (self, bytes_svckeymud):
		"""Given a sequence of service keymud bytes, produce the keys
		   concealed inside it and return the corrsponding Key objects
		   as a list.
		   
		   This is the inverse operation of TODO() defined on TODO.
		"""
		print ('DEBUG: Now in %r.key_fromservice()' % self.ctx)
		self.have_kipservice ()
		print ('DEBUG: Entering _s.kipservice_frommap()')
		kids = [ ]
		assert _s.kipservice_frommap (self.ctx, bytes_svckeymud, kids)
		print ('DEBUG: Departed _s.kipservice_frommap() with len(kids) = %d' % len (kids))
		print ('DEBUG: Extracted key ids %r from map' % kids)
		# raise NotImplementedError ("Incomplete code: key_fromservice()")
		return [ Key (self, kid) for kid in kids ]

	def key_fromkeytab (self, keynr, alg, domain, service=None, hostname=None, ktname=None):
		"""Lookup a key in a keytab, based on the keynr/alg and the domain
		   and, if non-standard, the hostname.  The ktname can be set to a
		   specific key table if the usual search mechanisms are considered
		   to not work for the application at hand.
		"""
		tabkidp = _c.new_keyid ()
		assert _c.kipkey_fromkeytab (self.ctx, ktname, service, hostname, domain, keynr, alg, tabkidp)
		tabkid = _c.keyid_value (tabkidp)
		return Key (self, tabkid)

	def claim_keynr (self):
		"""Claim a key number, even before having a keyid setup for it.
		   A new key number is returned, and an emmpty list of keyids is
		   setup as a form of claim.
		"""
		keynr = 0
		while keynr in self.keymap:
			keynr += 1
		self.keymap [keynr] = [ ]
		return keynr

	def have_kipservice (self, stop=False):
		"""Ensure that the service has started (or stopped).
		   This is called from various places that demand KIP Service
		   (or not, if they ceased their demand).
		"""
		global loaded_service
		assert loaded_service, 'Failed to load KIP Service wrapper'
		if self.on_kipservice:
			if stop:
				_s.kipservice_stop  (self.ctx)
				self.on_kipservice = False
		else:
			if not stop:
				assert _s.kipservice_start (self.ctx)
				self.have_kipservice_variables ()
				self.on_kipservice = True

	def have_kipservice_variables (self):
		"""Load the values for KIP_REALM, KIPSERVICE_CLIENT_REALM,
		   KIPSERVICE_CLIENTUSER_LOGIN and KIPSERVICE_CLIENTUSER_ACL;
		   substitute defaults in case of doubt.
		"""
		import os
		krealm = os.getenv ('KIP_REALM')
		crealm = os.getenv ('KIPSERVICE_CLIENT_REALM')
		clogin = os.getenv ('KIPSERVICE_CLIENTUSER_LOGIN')
		calias = os.getenv ('KIPSERVICE_CLIENTUSER_ACL')
		krealm = krealm or crealm or 'example.com'
		crealm = crealm or krealm
		clogin = clogin or calias or 'nobody'
		calias = calias or clogin
		assert _s.kipservice_set_client_realm     (self.ctx, crealm)
		# assert _s.kipservice_set_server_realm   (self.ctx, krealm)
		assert _s.kipservice_set_clientuser_login (self.ctx, clogin)
		assert _s.kipservice_set_clientuser_acl   (self.ctx, calias)

