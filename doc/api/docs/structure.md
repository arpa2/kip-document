# Anatomy of a KIP Document

> *The general structure of a KIP Document is a sequence of CBOR chunks
> with a given interpretation.*

KIP Documents form a concatenation of [CBOR elements](../cbor) that may continue
up to the end-of-file.

The [magic header](../header) at the start of a KIP Document is a readable
string that parse as two CBOR objects that each hold a binary string.

After the header, every [CBOR chunk](../cbor] is an array, starting with an
integer tag.  This tag determines the interpretation as a particular
chunk type.

