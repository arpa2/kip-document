# Public keys with `PubKey` chunks

> *Public Keys introduce a symmetric key.*

Most cryptographic work is done with symmetric keys, and the heavier-weight
public-key crypto is predominantly used to introduce symmetric keys.
Such secondary symmetric keys are commonly referred to as a
session key, for use in otherwise normal KIP Core operations.
Any Validator relying on this key need not be a `PubSig`; it may
just as well be `SigMud` or `SvcSigMud`.  Various public key systems
are supported, and named in a string.
