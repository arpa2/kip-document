# Python3 API for KIP Document

> *The API offers a playground for in-memory KIP Documents.*

We will not go into the details of the API here.  The Python3
inline documentation is a much easier way to get to this.  What
we will do here, is give a few examples, with identifiers to
get you started.


## KIP Document from scratch

Constructing a KIP Document holding just a simple string, and
saving it to a file named `myfile.kip`:

```python
from arpa2.kip.document import Document, Lit

my1st = Document ()
my1st.append (Lit ('Hello Hot World'))

with open ('myfile.kip', 'wb') as fh:
	my1st.to_cbor_file (fh)
```

Note that `my1st` inherits from `list`, which is how it can use
the `append()` operation, and many others.  This may also break
the consistency of the KIP Document, so be mindful with that.


## KIP Document from file

Reading back a file `myfile.kip` and printing the literal content
that was stored in each `Lit` chunk:

```python
with open ('myfile.kip', 'rb') as fh:
	my2nd = Document.from_cbor_file (fh)

for chunk in my2nd:
	if type (chunk) is Lit:
		print (chunk.get_content ())

```


